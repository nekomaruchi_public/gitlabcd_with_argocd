#/bin/bash

IMGVALUE=$1
if [ -z "$IMGVALUE" ]; then echo "There is no value for change image tag"; exit 0; fi
PROJECTNAME=$2
if [ ! -z "PROJECTNAME" ]; then PROJECTNAME="${PROJECTNAME}/"; fi

# read the yml template from a file and substitute the string 
# {{MYVARNAME}} with the value of the MYVARVALUE variable
template=`cat "${PROJECTNAME}pod.yaml.template" | sed "s/{{IMGTAGVALUE}}/$IMGVALUE/g"`

# apply the yml with the substituted value
echo "$template" > "${PROJECTNAME}/pod.yaml"
echo "$template"

