# Structure

```
cdp-backend-services
$PROJECTNAME1
$PROJECTNAME2
...etc
```
These folder's name are services which need to be deployed on Kubernetes, they also need to be same as original project name.

# Current structure of workflow 
![current workflow](https://storage.googleapis.com/zenn-user-upload/syjdas7c7cgto1ciayhlmmlfmsnj)

This repostory would be deployed on Kubernetes go through these steps;
```
git clone $MANIFEST_REPO
yq manifest (modify manifest)
git push 
```
If GitLab-CI started running when developing projects was merged/push,   
this project would also be pull when GitLab-CI was running, and modify their latest image tag name in the folder,   
which is same as the side project name, then Gitlab-CI would commit and push this repository back.  

After all the steps was done,   
ArgoCD (if integreted) which is the CD software automatically monitor specific project (if set well) every moment will check the updating and upgrade the application to Kubernetes.

# File explain
```
img_changer.sh
```
This bash-script is the executable file one, intended for modifying Kubernetes yaml file .
  
```
img_changer2.sh
Same as img_change.sh
Only for multiple containers pod. 
```
Literally, for multiple containers pod.

#  
```
project_secret_importer.sh
```
Could be executed like  
``` sh project_secret_importer.sh```

It is the most important script to import gitlab deploying secret to K8s cluster.  
Before integrated with K8s, before executed this script,   
we have to find the **gitlabproject_authkey** authorization token in **variable** of **CI/CD** in project's repostory.  

```
This gitlabproject_authkey variable won't exist originally,  
it sould be the base64 token. Please feel free to add in Variable in repostory.  
The DeployToken needs to be generated first, and put the variable with the command-line:  
echo -n "REGISTRY_USERNAME:REGISTRY_PASSWORD" | base64   

Then this will be the authkey.
```

Please edit this script, and add value after gitlabproject_authkey if this file.   
``` 
gitlabproject_authkey=XXXXXXXXXXXXXXXXXXX
```
**save, and re-run this script** at K8s environment, it will create secret for you.

After imported secret, we have to check the secret entity in Kubernetes, to make sure we already imported secret successful.

# About Ingress routing rule

`externalservice.yaml` and `ingress.yaml` are file for setting routing on K8s,  
If you already got a Public IP for Loadbalance of Kubernetes,   
you can just apply these two file on Kubernetes after deployed Ingress, 

Note that `externalservice.yaml` is an ExternalName service of Kubernetes, 
you can apply it depends on your policy of the scinario.

`gitlab-ci.yaml` is the part of script which need to be add in gitlab-ci.yml under the original repostory.