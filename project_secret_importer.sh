#!/bin/bash


keypath='/opt/project_gitauthkey'
gitlabproject_authkey=""
if [ -z "$cdp_gitlabproject_authkey" ]; then echo "Please find \"gitlabproject_authkey\" variable from project, modify this shell script or run this script with argument."; exit 0; fi 
echo '{"auths": {"registry.gitlab.com": {"auth": "'${gitlabproject_authkey}'"}}}' > ${keypath}

kubectl create secret generic gitlab-registry --from-file=.dockerconfigjson=${keypath} --type=kubernetes.io/dockerconfigjson -n develop

