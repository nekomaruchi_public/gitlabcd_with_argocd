#/bin/bash

IMGVALUE=$1
if [ -z "$IMGVALUE" ]; then echo "There is no value for change image tag"; exit 0; fi
PROJECTNAME=$2
if [ ! -z "$PROJECTNAME" ]; then PROJECTNAME="${PROJECTNAME}/"; fi
CHANGELIST=$3
#if [ -z "$CHANGELIST" ]; then echo "Please provide string list for building project"; exit 0; fi

if [ ! -z $CHANGELIST ] && [ -f $CHANGELIST ] ;then
  expression=""
  while IFS= read -r line ;do
  
   if [ -z "$expression" ] ;then
     expression="${line}"":"
   else 
     expression=${expression}'\|'${line}":" 
   fi
  
  done < $CHANGELIST
  temp=`sed -e "s/\($expression\).*/\1${IMGVALUE}/g" ${PROJECTNAME}pod.yaml.template`
  echo "$temp" > ${PROJECTNAME}pod.yaml
  cat ${PROJECTNAME}pod.yaml
  sed -i 's/{{IMGTAGVALUE}}/release/g' ${PROJECTNAME}pod.yaml
  cat ${PROJECTNAME}pod.yaml

else

  template=`cat "${PROJECTNAME}pod.yaml.template" | sed "s/{{IMGTAGVALUE}}/release/g"`
  
  # apply the yml with the substituted value
  echo "$template" > "${PROJECTNAME}/pod.yaml"
  echo "$template"
fi
